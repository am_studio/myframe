<style>
    @import url(debug/css/bootstrap.css);
    @import url(debug/css/style.css);
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js" type="text/javascript"></script>

<div id="debug-bar" class="debug-bar debug-minimazed">
    <!-- Main wrapper  -->
    <div id="debug-main-wrapper">
        <!-- header header  -->
        <script>
            function openTab(evt, name) {
                var i, tabcontent, tablinks;
                tabcontent = document.getElementsByClassName("debug-tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("debug-tablinks");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" debug-active", "");
                }
                document.getElementById(name).style.display = "block";
                evt.currentTarget.className += " debug-active";
            }

            function debug(show) {
                var item = document.getElementById('debug-bar');
                item.className = item.className.replace(' debug-minimazed','');

                if(!show) {
                    item.className += ' debug-minimazed';
                }
                return false;
            }
        </script>

        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="debug-page-wrapper" style="padding: 0;">
            <!-- Bread crumb -->
            <div class="debug-row debug-page-titles">
                <div class="debug-text-left">
                    <div class="debug-tab">
                        <button class="debug-tablinks debug-active" onclick="openTab(event, 'debug-home')">Home</button>
                        <button class="debug-tablinks" onclick="openTab(event, 'debug-db')">Database</button>
                        <button class="debug-tablinks" onclick="openTab(event, 'debug-opcache')">OPCache</button>
                    </div>
                    <div class="debug-short">
                        <ul>
                            <li><strong>Response time</strong>: <?php echo profiler()->get('response_time'); ?>ms</li> ::
                            <li><strong>Framework time</strong>: <?php echo profiler()->get('framework_time'); ?>ms</li> ::
                            <li><strong>DB result time</strong>: <?php echo profiler()->get('db_result_time'); ?>ms</li>
                        </ul>
                    </div>
                </div>
                <div class="debug-close">
                    <a href="#" onclick="return debug(false)"><i class="fa fa-window-close"></i></a>
                </div>
                <div class="debug-show">
                    <a href="#" onclick="return debug(true)"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <!-- End Bread crumb -->

            <!-- Container fluid  -->
            <div id="debug-home" class="debug-container-fluid debug-tabcontent" style="display: block">
                <!-- Start Page Content -->
                <div class="debug-row">
                    <div class="debug-col-md-3">
                        <div class="debug-card">
                            <div class="debug-media">
                                <div class="debug-media-left debug-media debug-media-middle">
                                    <span><i class="fa fa-dashboard debug-text-success"></i></span>
                                </div>
                                <div class="debug-media-body debug-media-text-right">
                                    <h2><?php echo profiler()->get('response_time'); ?>ms</h2>
                                    <p>Response time</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="debug-col-md-3">
                        <div class="debug-card">
                            <div class="debug-media">
                                <div class="debug-media-left debug-media debug-media-middle">
                                    <span><i class="fa fa-code debug-text-info"></i></span>
                                </div>
                                <div class="debug-media-body debug-media-text-right">
                                    <h2><?php echo profiler()->get('framework_time'); ?>ms</h2>
                                    <p>Framework start</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="debug-col-md-3">
                        <div class="debug-card">
                            <div class="debug-media">
                                <div class="debug-media-left debug-media debug-media-middle">
                                    <span><i class="fa fa-server debug-text-warning"></i></span>
                                </div>
                                <div class="debug-media-body debug-media-text-right">
                                    <h2><?php echo profiler()->totalRam(); ?></h2>
                                    <p>Allocated to one PHP script</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="debug-col-md-3">
                        <div class="debug-card">
                            <div class="debug-media">
                                <div class="debug-media-left debug-media debug-media-middle">
                                    <span><i class="fa fa-microchip debug-text-danger"></i></span>
                                </div>
                                <div class="debug-media-body debug-media-text-right">
                                    <h2><?php echo profiler()->get('memory_framework'); ?>KB</h2>
                                    <p>Framework RAM (1/R) used</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="debug-row debug-bg-white">
                    <!-- column -->
                    <div class="debug-col-lg-8">
                        <div class="debug-card">
                            <div class="debug-card-body">
                                <h4 class="debug-card-title">Framework speed</h4>
                                <div style="height: 250px">
                                    <canvas id="debug-time-chart"></canvas>
                                </div>

                                <script>
                                    var dataIndexes = ['Framework start', 'Request parsed', 'DB result', 'Other'];

                                    var horizontalBarChartData = {
                                        labels: ['Time'],
                                        datasets: [{
                                            label: dataIndexes[0],
                                            backgroundColor: ["#fc6180"],
                                            data: [
                                                <?php echo profiler()->get('framework_time'); ?>,
                                            ]
                                        },{
                                            label: dataIndexes[1],
                                            backgroundColor: ["#62d1f3"],
                                            data: [
                                                <?php echo profiler()->get('request_time'); ?>,
                                            ]
                                        },{
                                            label: dataIndexes[2],
                                            backgroundColor: ["#26dad2"],
                                            data: [
                                                <?php echo profiler()->get('db_result_time'); ?>
                                            ]
                                        },{
                                            label: dataIndexes[3],
                                            backgroundColor: ["#afafaf"],
                                            data: [
                                                <?php echo profiler()->otherTime(); ?>
                                            ]
                                        }]
                                    };

                                    window.onload = function() {
                                        var ctx = document.getElementById('debug-time-chart').getContext('2d');

                                        window.myHorizontalBar = new Chart(ctx, {
                                            type: 'horizontalBar',
                                            data: horizontalBarChartData,
                                            options: {
                                                tooltips: {
                                                    callbacks: {
                                                        label: function(tooltipItem) {
                                                            return tooltipItem.xLabel + 'ms - ' + dataIndexes[tooltipItem.datasetIndex];
                                                        }
                                                    }
                                                },
                                                elements: {
                                                    rectangle: {
                                                        borderWidth: 2
                                                    }
                                                },
                                                responsive: true,
                                                maintainAspectRatio: false,
                                                scales: {
                                                    xAxes: [{
                                                        stacked: true
                                                    }],
                                                    yAxes: [{
                                                        stacked: true
                                                    }]
                                                }
                                            }
                                        });
                                    };
                                </script>
                            </div>
                        </div>
                    </div>
                    <!-- column -->

                    <!-- column -->
                    <div class="debug-col-lg-4">
                        <div class="debug-card">
                            <div class="debug-card-body debug-browser">
                                <p>Server RAM load <span class="debug-pull-right"><?php echo round(profiler()->getRAMLoad()) ?>%</span></p>
                                <div class="debug-progress ">
                                    <div style="width: <?php echo profiler()->getRAMLoad() ?>%; height:8px;" class="debug-progress-bar debug-bg-danger debug-progress-animated"></div>
                                </div>
                                <p>Server CPU load<span class="debug-pull-right"><?php echo round(profiler()->getCPULoad()) ?>%</span></p>
                                <div class="debug-progress ">
                                    <div style="width: <?php echo profiler()->getCPULoad() ?>%; height:8px;" class="debug-progress-bar debug-bg-warning debug-progress-animated"></div>
                                </div>
                                <p>PHP: <span class="debug-pull-right"><?php echo profiler()->getPHPVer() ?></span></p>
                                <p>PHP OP cache<span class="debug-pull-right"><?php  echo profiler()->get('opcache')['status'] ?></span></p>
                                <p>Server<span class="debug-pull-right"><?php echo profiler()->get('software'); ?></span></p>
                                <p>Server IP<span class="debug-pull-right"><?php echo profiler()->get('ip'); ?></span></p>
                                <p>Server OS<span class="debug-pull-right"><?php echo profiler()->getOS(); ?></span></p>
                                <p>Method<span class="debug-pull-right"><?php echo profiler()->get('method'); ?></span></p>
                                <p>Protocol<span class="debug-pull-right"><?php echo profiler()->get('protocol'); ?></span></p>
                                <p>Interface<span class="debug-pull-right"><?php echo profiler()->get('interface'); ?></span></p>
                                <p>Scheme<span class="debug-pull-right"><?php echo profiler()->get('scheme'); ?></span></p>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                </div>

                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->

            <div id="debug-db" class="debug-container-fluid debug-tabcontent">

            </div>

            <div id="debug-opcache" class="debug-container-fluid debug-tabcontent">

            </div>
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
</div>