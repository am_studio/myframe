<?php
namespace src\Framework;

/**
 * Class Config
 * @package src\Framework
 */
final class Config {
    /**
     * @var null
     */
    private static $instance  = null;
    /**
     * @var array
     */
    private $settings = [];

    /**
     * @param string $file
     * @return null|Config
     */
    public static function getInstance(string $file) {
        if(self::$instance === null) {
            self::$instance = new self;
            self::$instance->settings[$file] = include("config/$file.php");
        }
        return self::$instance;
    }

    /**
     * @param string $key
     * @param string $file
     * @return bool
     */
    public static function has(string $key, string $file = 'app') {
        return isset(self::getInstance($file)->settings[$file][$key]);
    }

    /**
     * @param string $key
     * @param string $file
     * @return null|mixed
     */
    public static function get(string $key, string $file = 'app') {
        return self::getInstance($file)->settings[$file][$key] ?? null;
    }
}