<?php
namespace src\Framework\Exception;

class InvalidTokenException extends \LogicException {
    public function __construct() {
        parent::__construct('CSRF token validation failed');
    }
}