<?php
namespace src\Framework\Exception;

class RequestNotMatchedException extends \LogicException {
    private $request;

    public function __construct(array $request) {
        $this->request = $request;
        parent::__construct('Matches not found');
    }

    /**
     * @return array
     */
    public function getRequest(): array {
        return $this->request;
    }
}