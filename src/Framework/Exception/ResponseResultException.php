<?php
namespace src\Framework\Exception;

class ResponseResultException extends \LogicException {
    public function __construct() {
        parent::__construct('Controller not return instance of IResponse');
    }
}