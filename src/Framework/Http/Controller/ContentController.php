<?php
namespace src\Framework\Http\Controller;

use src\Framework\Core\Controller\Controller;
use src\Framework\Core\Driver\Profiler;
use src\Framework\Models\User;

class ContentController extends Controller {
    public function index() {
//        $redis = new Redis();
//        $redis->connect('127.0.0.1', 6379);

//        $redis->flushDb();
//        $redis->set('key', 'value');

//        $task = [];
//
//        for ($i = 0; $i < 25; $i++) {
//            $task[] = [
//                'name' => 'Awesome task name',
//                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//                'budget' => 14,
//                'type' => 'In Person',
//                'location' => 'location.key',
//            ];
//        }
//
//        for($i = 0; $i < 3000; $i++) {
//            $redis->set('user.' . $i, json_encode([
//                'user' => [
//                    'name' => 'User',
//                    'created_at' => '18.06.2018'
//                ],
//                'task' => $task
//            ]));
//        }

//        $redis->get('key.1000');

        // Start watch
        Profiler::startWatch(Profiler::DB_RESULT_TIME);
        //$user = User::findById(1);
        // End watch
        Profiler::stopWatch(Profiler::DB_RESULT_TIME);
        return $this->render('hello', ['var' => 'myvar']);
    }
}