<?php
namespace src\Framework;

use src\Framework\Core\Driver\Profiler;
use src\Framework\Exception\ResponseResultException;
use src\Framework\Core\Router\RouteLoader;
use src\Framework\Core\Router\UriHandler;
use src\Framework\Exception\InvalidTokenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

/**
 * Class Kernel
 * @package src\Framework
 */
final class Kernel {
    /**
     * @param string $method
     * @param null|string $token
     */
    private static function crsfCheck(string $method, ?string $token) {
        if(config('csrf_token')) {
            // Get user token
            $csrf = $_SESSION['csrf_token'] ?? null;

            // Get time
            $time = time();

            // Get base token
            $baseToken = config('csrf_token_base_key');

            // Set new token
            $_SESSION['csrf_token'] = [
                'token' => md5("{$baseToken}{$time}"),
                'time' => $time
            ];

            // Check 'sending' methods
            if ($method === 'POST' ||
                $method === 'PUT' ||
                $method === 'DELETE' ||
                $method === 'PATCH') {

                // Check token
                if($token !== $csrf['token']) {
                    throw new InvalidTokenException();
                }
                // Check token time
                if($time - $csrf['time'] >= config('csrf_token_time')) {
                    throw new InvalidTokenException();
                }
            }
        }
    }

    /**
     * Handle Request & Send Response
     */
    public static function start() {
        // Start watch
        Profiler::startWatch(Profiler::REQUEST_TIME);

        // Run session
        session_start();

        // Get request
        $request = Request::createFromGlobals();
        $context = new RequestContext();
        $context->fromRequest($request);

        // Check token
        self::crsfCheck($request->getMethod(), $request->get('token'));

        // End watch
        Profiler::stopWatch(Profiler::REQUEST_TIME);

        // Get route collection and check matches
        $matcher = new UrlMatcher(RouteLoader::getCollection(), $context);

        // Get params
        $parameters = $matcher->match($context->getPathInfo());

        // Get response
        $response = (new UriHandler())->match($request, $parameters);

        // Validate response
        if($response instanceof Response === false) {
            throw new ResponseResultException();
        }

        // Draw response
        $response->send();

        // End watch
        Profiler::stopWatch(Profiler::RESPONSE_TIME);
        Profiler::stopWatch(Profiler::MEMORY_FRAMEWORK, Profiler::TYPE_MEM_USAGE);

        // Draw some debug data
        if(config('is_debug')) {
            include_once('resources/views/debug/bar.php');
        }
    }
}