<?php
namespace src\Framework\Core\Driver;

/**
 * Class Profiler
 * @package src\Framework\Core\Driver
 */
final class Profiler {
    public const FRAMEWORK_TIME = 'framework_time';
    public const REQUEST_TIME = 'request_time';
    public const DB_RESULT_TIME = 'db_result_time';
    public const RESPONSE_TIME = 'response_time';

    public const MEMORY_TOTAL = 'memory_total';
    public const MEMORY_FRAMEWORK = 'memory_framework';

    public const TYPE_TIME_ELAPSED = 0;
    public const TYPE_MEM_USAGE = 1;

    /**
     * @var null|Profiler
     */
    private static $instance = null;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @return null|Profiler
     */
    public static function getInstance() {
        if(self::$instance === null) {
            self::$instance = new self;

            self::$instance->data['software'] = $_SERVER['SERVER_SOFTWARE'];
            self::$instance->data['ip'] = $_SERVER['SERVER_ADDR'];
            self::$instance->data['method'] = $_SERVER['REQUEST_METHOD'];
            self::$instance->data['protocol'] = $_SERVER['SERVER_PROTOCOL'];
            self::$instance->data['interface'] = $_SERVER['GATEWAY_INTERFACE'];
            self::$instance->data['scheme'] = $_SERVER['REQUEST_SCHEME'];
            self::$instance->data['opcache']['status'] = '-';

            if(extension_loaded('opcache')) {
                self::$instance->data['opcache']['status'] = opcache_get_status(false);
            }
        }
        return self::$instance;
    }


    /**
     * @param $name
     * @param int $type
     * @param number|bool $val
     */
    public static function startWatch($name, $type = self::TYPE_TIME_ELAPSED, $val = false) {
        $instance = self::getInstance();

        if($type === self::TYPE_TIME_ELAPSED) {
            $start = is_numeric($val) ? $val : microtime(true);
        } else {
            $start = is_numeric($val) ? $val : memory_get_usage();
        }
        $instance->data[$name] = $start;
    }

    /**
     * @param $name
     * @param int $type
     */
    public static function stopWatch($name, $type = self::TYPE_TIME_ELAPSED) {
        $instance = self::getInstance();

        if(isset($instance->data[$name]) === true) {
            if($type === self::TYPE_TIME_ELAPSED) {
                $instance->data[$name] = round((microtime(true) - $instance->data[$name]) * 1000);
            } else {
                $instance->data[$name] = round((memory_get_usage() - $instance->data[$name]) / 1024);
            }
        }
    }

    /**
     * @param string $name
     * @param number $time
     */
    public static function withTime($name, $time) {
        self::getInstance()->data[$name] = round((microtime(true) - $time) * 1000);
    }

    /**
     * @param string $name
     * @param number $ram
     */
    public static function withRam($name, $ram) {
        self::getInstance()->data[$name] = round((memory_get_usage() - $ram)) / 1024;
    }

    /**
     * @param $name
     * @return number|null
     */
    public static function get($name) {
        $instance = self::getInstance();
        if(isset($instance->data[$name]) === true) {
            return $instance->data[$name];
        }
        return null;
    }

    /**
     * @return float
     */
    public static function totalRam() {
        return ini_get('memory_limit');
    }

    /**
     * @return number
     */
    public static function otherTime() {
        $instance = self::getInstance();
        return $instance->data[self::RESPONSE_TIME] - (
            $instance->data[self::FRAMEWORK_TIME] +
                $instance->data[self::REQUEST_TIME] +
                $instance->data[self::DB_RESULT_TIME]);
    }

    /**
     * @return string
     */
    public static function getPHPVer() {
        return PHP_MAJOR_VERSION . '.' .PHP_MINOR_VERSION;
    }

    /**
     * @return string
     */
    public static function getOS() {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ? 'Windows' : 'Linux';
    }

    /**
     * @return mixed
     */
    public static function getRAMLoad() {
        $instance = self::getInstance();

        if(isset($instance->data['ram_load'])) {
            return $instance->data['ram_load'];
        }

        if (substr(PHP_OS, 0, 3) === 'WIN') {
            $cmd = 'wmic OS get TotalVisibleMemorySize';
            exec($cmd, $lines, $ret);
            if($ret === 0) {
                $cmd = 'wmic OS get FreePhysicalMemory';
                exec($cmd, $lines, $ret);
                if($ret === 0) {
                    $instance->data['ram_load'] = 100 - $lines[4]/$lines[1]*100;
                }
            }
        } else {
            $free = shell_exec('free');
            $free = (string)trim($free);
            $free_arr = explode("\n", $free);
            $mem = explode(" ", $free_arr[1]);
            $mem = array_filter($mem);
            $mem = array_merge($mem);
            $instance->data['ram_load'] = $mem[2]/$mem[1]*100;
        }
        return $instance->data['ram_load'];
    }

    /**
     * @return mixed
     */
    public static function getCPULoad() {
        $instance = self::getInstance();

        if(isset($instance->data['cpu_load'])) {
            return $instance->data['cpu_load'];
        }

        if (substr(PHP_OS, 0, 3) === 'WIN') {
            $cmd = 'wmic cpu get loadpercentage';
            exec($cmd, $lines, $ret);
            if($ret === 0) {
                $instance->data['cpu_load'] = $lines[1];
            }
        } else {
            $instance->data['cpu_load'] = sys_getloadavg();
        }
        return $instance->data['cpu_load'];
    }
}