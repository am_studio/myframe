<?php
namespace src\Framework\Core\Driver;

use src\Framework\Core\Models\BaseModel;

/**
 * Class BaseDriver
 * @package src\Framework\Core\Driver
 */
abstract class BaseDriver {
    /**
     * @var bool
     */
    protected $shard = false;

    /**
     * @var int
     */
    protected $num_servers = 3;

    /**
     * @param $id
     * @return number
     */
    protected abstract function shard($id) : number;

    /**
     * @return array
     */
    public abstract static function all() : array;

    /**
     * @param $id
     * @return null|BaseModel
     */
    public abstract static function find($id) :? BaseModel;

    /**
     * @param $name
     * @param $value
     * @param string $operator
     * @return BaseDriver
     */
    public abstract static function where($name, $value, $operator = '=') : self;

    /**
     * @param $value
     * @return BaseDriver
     */
    public abstract static function limit($value) : self;

    /**
     * @param $value
     * @return BaseDriver
     */
    public abstract static function offset($value) : self;

    /**
     * @return array
     */
    public abstract static function get() : array;

    /**
     * @param $value
     * @return bool
     */
    public abstract static function create($value) : bool;

    /**
     * @return bool
     */
    public abstract function save() : bool;

    /**
     * @return bool
     */
    public abstract function update() : bool;

    /**
     * @return bool
     */
    public abstract function delete() : bool;
}