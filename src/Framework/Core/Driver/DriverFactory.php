<?php
namespace src\Framework\Core\Driver;

use src\Framework\Config;

class DriverFactory {
    protected static $instance = null;
    public const FLAG_OPERATION_READ = 'read';
    public const FLAG_OPERATION_WRITE = 'write';
    public const FLAG_CLUSTER = 'cluster';
    public const FLAG_DEFAULT_DRIVER = 'db_driver';

    public static function balancing($hosts) {
       return $hosts[rand(0, sizeof($hosts) - 1)];
    }

    public static function connection($driver = self::FLAG_DEFAULT_DRIVER, $operation = self::FLAG_OPERATION_READ) {
        // Check instance
        if(static::$instance === null) {
            $driver = Config::get($driver);

            if(isset($driver[$operation])) {
                $driver = $driver[$operation];
            }

            if(isset($driver[self::FLAG_CLUSTER])) {
                $driver = self::balancing($driver[self::FLAG_CLUSTER]);
            }

            switch ($driver['name']) {
                case 'redis':
                    static::$instance = new RedisDriver();
                    static::$instance->connect($driver['host'], $driver['port']);
                    break;
            }
        }
        return static::$instance;
    }
}