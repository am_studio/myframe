<?php
namespace src\Framework\Core\Driver;

use Redis;

class RedisDriver implements iDriver {
    protected $instance = null;

    public function get($hash): string {
       $data = $this->instance->get($hash);
       return $data === "" ? false : $data;
    }

    public function set($hash, $data): boolean {
        // TODO: Implement set() method.
    }

    public function has($hash): boolean {
        return $this->instance->exists($hash) !== false;
    }

    public function create($data): boolean {
        // TODO: Implement create() method.
    }

    public function all($hash): array {
        $keys = $this->instance->keys($hash);
        $values = $this->instance->mGet($keys);
        return array_combine($keys, $values);
    }

    public function take($offset, $limit): array {
        return [];
    }

    public function size() : int {
        return $this->instance->dbSize();
    }

    public function connect($host, $port) {
        $this->instance = new Redis();
        $this->instance->connect($host, $port);
        return $this->instance;
    }
}