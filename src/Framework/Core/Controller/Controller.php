<?php
namespace src\Framework\Core\Controller;

//use src\Framework\Http\Response;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;

abstract class Controller {
    /**
     * @param $viewPath
     * @param array $arguments
     * @return Response
     */
    public final function render($viewPath, array $arguments = []) {
        $filesystemLoader = new FilesystemLoader('resources/views/%name%.php');
        $template = new PhpEngine(new TemplateNameParser(), $filesystemLoader);
//        $template->addGlobal('ga_tracking', 'UA-xxxxx-x');

        return new Response($template->render($viewPath, $arguments));
    }

    public final function json(array $data) {
        return new Response(json_encode($data));
    }
}