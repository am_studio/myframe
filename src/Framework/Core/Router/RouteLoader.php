<?php
namespace src\Framework\Core\Router;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteLoader
 * @package src\Framework\Core\Router
 */
final class RouteLoader {
    /**
     * @return RouteCollection
     */
    public static function getCollection() {
        // Init route collection
        $collection = new RouteCollection();

        // Get route list
        $list = include('routes/web.php');

        // Parse to Symfony format
        foreach ($list as $key => $item) {
            // Check if route have name
            if(isset($item['name']) === false) {
                $item['name'] = strtolower(str_replace('\\', '_', $item['controller']));
            }

            // Add to Symfony collection
            $collection->add(
                $item['name'],
                (new Route(
                    $key,
                    ['_controller' => $item['controller']]
                ))->setMethods($item['method']));
        }

        // Return new collection
        return $collection;
    }
}