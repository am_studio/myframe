<?php
namespace src\Framework\Core\Router;

use ReflectionMethod;
use src\Framework\Exception\RequestNotMatchedException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UriHandler
 * @package src\Framework\Core\Router
 */
class UriHandler {
    /**
     * @param Request $request
     * @param array $parameters
     * @return array
     */
    public function match(Request $request, array $parameters) {
        // Split parameters
        $split = explode('::', $parameters['_controller']);

        // Get class name
        $className = $split[0];

        // Get method name
        $method = $split[1];

        // Class path
        $classPath = "\\src\\Framework\\Http\\Controller\\$className";

        // Get class
        $class = new $classPath;

        // Find method in class
        if(method_exists($class, $method)) {
            // Filtered request (remove default '_controller' and '_route' params)
            $parameters = array_filter($parameters, function ($item) {
                return $item[0] !== '_';
            }, ARRAY_FILTER_USE_KEY);

            // Get information about class function
            $reflection = new ReflectionMethod($class, $method);

            // Get method params
            $params = $reflection->getParameters();

            // Check if params count min:0
            if(isset($params[0]) === true) {
                // Get first params item class name
                if($params[0]->getClass() !== null && $params[0]->getClass()->getName() === Request::class) {
                    // Add $request property
                    array_unshift($parameters, $request);
                }
            }
            return call_user_func_array([$class, $method], $parameters);
        }

        throw new RequestNotMatchedException($parameters);
    }
}