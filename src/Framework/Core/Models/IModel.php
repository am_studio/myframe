<?php
namespace src\Framework\Core\Models;

interface iModel {
    public static function find($ids) :? array;
    public static function findById($id) :? self;
    public static function all() : array;
    public static function take($offset, $limit) : array;
//    public static function findOrCreate(integer $id, array $default);
//    public static function create(array $data);
//    public static function search(mixed $ids);
//    public static function filter(string $row, mixed $filterable);
//    public function save() : boolean;
//    public function size() : int;
//    public function remove() : boolean;
//    public function saveWithProtection() : boolean;
}