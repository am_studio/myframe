<?php
namespace src\Framework\Core\Models;

abstract class BaseModel {
    protected $attributes = [];
    protected $original = [];
}