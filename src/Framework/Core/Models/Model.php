<?php
namespace src\Framework\Core\Models;

use src\Framework\Core\Driver\DriverFactory;
use src\Framework\Core\Driver\Profiler;

class Model extends DriverFactory implements iModel {
    protected $id;
    protected $data;

    public static function find($ids) : array {
        if(is_array($ids) === true) {
            $data = [];
            foreach ($ids as $id) {
                array_push($data, static::findById($id));
            }
            return $data;
        } else {
            return [ static::findById($ids) ];
        }
    }

    public static function findById($id, $driver = parent::FLAG_DEFAULT_DRIVER) :? iModel {
        $connection = static::connection($driver);
        $data = $connection->get(static::getKey($id));

        if ($data !== false) {
            $model = new static();
            $model->id = $id;
            $model->data = $data;
            return $model;
        }
    }

    public static function all($driver = parent::FLAG_DEFAULT_DRIVER) : array {
        $connection = static::connection($driver);
        $data = $connection->all(static::getKey('*'));
        $models  = [];

        foreach ($data as $key => $item) {
            $model = new static();
            $model->id = explode('.', $key)[1];
            $model->data = $item;
            $models[] = $model;
        }
        return $models;
    }

    public static function take($offset, $limit): array {

    }

    public static function getKey($id) : string {
        $name = strtolower(substr(strrchr(static::class, "\\"), 1));
        return "$name.$id";
    }

    public function getId() {
        return $this->id;
    }

    public function __get($name) {
        if(isset($this->data)) {
            if (is_string($this->data)) {
                $this->data = json_decode($this->data, true);
            }
            if(isset($this->data[$name])) {
                if(method_exists($this, "get$name")) {
                    return call_user_func([$this, "get$name"], $this->data[$name]);
                } else {
                    return $this->data[$name];
                }
            }
        }

        if(method_exists($this, "get$name")) {
            return call_user_func([$this, "get$name"]);
        }
        return null;
    }

    public function __set($name, $attr) {
        if(method_exists($this, "set$name")) {
            call_user_func([$this, "set$name"], $attr);
        }
    }
}