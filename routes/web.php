<?php
return [
    '/' => [
        'controller' => 'ContentController::index',
//        'name' => 'content_index',
        'method' => ['GET'],
    ],
    '/foo/{id}/{name}' => [
        'controller' => 'Admin\ContentController::index',
//        'name' => 'admin_content_index',
        'method' => ['GET', 'POST'],
    ],
];