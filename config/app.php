<?php

return [
    'is_debug' => true,

    'csrf_token' => true,
    'csrf_token_base_key' => '6a29622fe857553ab7c8e322d1c6de49',
    'csrf_token_time' => 3600, // sec

    'db_driver' => [
        'name' => 'redis',
        'host' => '127.0.0.1',
        'port' => '6379',
    ],

    /*'db_driver' => [
      'read' => [
          'cluster' => [
              [
                  'name' => 'redis',
                  'host' => '127.0.0.1',
                  'port' => '6379',
              ],
              [
                  'name' => 'redis',
                  'host' => '127.0.0.1',
                  'port' => '6379',
              ],
          ]
      ],
      'write' => [
          'name' => 'redis',
          'host' => '127.0.0.1',
          'port' => '6379',
      ],
  ],*/

    /*'db_driver' => [
        'name' => 'redis',
        'host' => '127.0.0.1',
        'port' => '6379',
    ],*/
];