<?php
/**
 * Created by PhpStorm.
 * User: Alex.M
 * Date: 02.07.2018
 * Time: 13:48
 */

function profiler() {
    return \src\Framework\Core\Driver\Profiler::getInstance();
}

/**
 * @param string $key
 * @param string $file
 * @return mixed
 */
function config(string $key, string $file = 'app') {
    return \src\Framework\Config::get($key, $file);
}

/**
 * @param mixed $digit
 * @param mixed $digit2
 * @return number
 */
function distance(mixed $digit, mixed $digit2) : number {
    return abs($digit - $digit2);
}