<?php
use src\Framework\Core\Driver\Profiler;
use src\Framework\Kernel;
use Symfony\Component\Debug\Debug;

// Start watch
$timeElapsed = microtime(true);
$memUsage = memory_get_usage();

// Change dir to src
chdir(dirname(__DIR__));

// User composer autoload
require 'vendor/autoload.php';

// Init debug
if(config('is_debug')) {
    Debug::enable();
} else {
    error_reporting(E_ALL);
    set_exception_handler(function($exception) {
//        $error = new \src\Framework\Http\Controller\ContentController()->
    });
}

// Start watch
Profiler::startWatch(Profiler::RESPONSE_TIME, Profiler::TYPE_TIME_ELAPSED, $timeElapsed);
Profiler::startWatch(Profiler::MEMORY_FRAMEWORK, Profiler::TYPE_MEM_USAGE, $memUsage);

// Set elapsed time
Profiler::withTime(Profiler::FRAMEWORK_TIME, $timeElapsed);

// Load framework
Kernel::start();

